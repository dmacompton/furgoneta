'use strict';

(function() {
	angular
		.module('ngFur', ['ngRoute'])
		.config(ngFurConfig)
		.controller('MainCtrl', MainCtrl);

	function ngFurConfig($routeProvider) {
		$routeProvider.when('/', {
			templateUrl: 'app/main/main.html',
			controller: 'MainCtrl',
			controllerAs: 'vm'
		}).when('/about', {
			templateUrl: 'app/about/about.html',
		}).when('/cart', {
			templateUrl: 'app/cart/cart.html',
			controller: CartCtrl
		}).when('/contact', {
			templateUrl: 'app/contact/contact.html',
			controller: ContactCtrl
		}).when('/menu', {
			templateUrl: 'app/menu/menu.html',
			controller: MenuCtrl
		}).when('/news', {
			templateUrl: 'app/news/news.html',
			controller: NewsCtrl
		}).when('/order', {
			templateUrl: 'app/order/order.html',
			controller: OrderCtrl
		});
	}

	function MainCtrl($scope) {
		$scope.count = 0;
		$scope.$on('$includeContentLoaded', function () {
			$scope.count++;
			if ($scope.count == 4) {
				setInterval(function(){
					$('#loader').hide();
				}, 100);
			}
		});
	}
	function CartCtrl($scope){
		setInterval(function(){
			$('#loader').hide();
		}, 100);
	}
	function ContactCtrl($scope){
		setInterval(function(){
			$('#loader').hide();
		}, 100);
	}
	function MenuCtrl($scope) {
		$scope.h1 = 'Меню';
		setInterval(function(){
			$('#loader').hide();
		}, 100);
	}
	function NewsCtrl($scope){
		setInterval(function(){
			$('#loader').hide();
		}, 100);
	}
	function OrderCtrl($scope){
		setInterval(function(){
			$('#loader').hide();
		}, 100);
	}
})();

$(document).ready(function () {
	var showAnimation = true,
		definitionSecondBlockHeight = 50,
		definitionBtnBlockUp = 350,
		definitionBtnBlockDown = false,
		definitionHambBlockDown = -200,
		definitionBotlBlockDown = 0,
		definitionWogBlockDown = 0,
		btnCoordinate = 0,
		showAnimation3block = true;

	/////////////////////////// FIRST BLOCK START ///////////////////////////
	hidePage()
	$(document).on('mousewheel DOMMouseScroll ', function (e) {
		if ($(window).scrollTop() == 0 && (e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0)) {
			// scroll up
			hidePage();
		} else if ($(window).scrollTop() == 0 && !(e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0)) {
			// scroll down
			showPage();
		}
	});
	$(document).on('keydown', function (e) {
		if (((e.which == 40 || e.which == 32 || e.which == 34) && $(window).scrollTop() == 0)) {
			showPage();
		}
	});
	$(document).on('click', '.arrow', function () {
		showPage();
		$(window).scrollTop(5);
	});
	function showPage() {
		$('#fullpage, footer').show();
		$('.main-first-block').addClass('anim').removeClass('anim-reverse');
		$('.main-second-block-row').addClass('animate-advantages');
	}

	function hidePage() {
		$('.main-first-block').removeClass('anim').addClass('anim-reverse');
		$('.main-second-block-row').removeClass('animate-advantages')
		$('#fullpage, footer').hide();
	}

	//-------------------------- FIRST BLOCK END --------------------------//
	var windowHeight = $(window).height();

	$(window).scroll(function () {
		var topOfWindow = $(window).scrollTop(),
			secBlckFixedPos = (windowHeight - 386) / 2 + 86 + $(document).scrollTop(),
			paddingTopSecBlock = ($('.main-second-block').height() - $('.main-second-block-row').height()) / 2 - 86 - 100;

		//header
		$('header').css('top', topOfWindow + 'px');
		$('.main-first-block').css('top', (topOfWindow + 86) + 'px');
		//header

		//FIRST BLOCK
		if ($(window).scrollTop() > 0) {
			showPage();
		} else {
			hidePage();
		}
		//FIRST BLOCK END

		/////////////////////////// SECOND BLOCK START ///////////////////////////
		// (высота окна - высота блока) / 2чтобПолучитьОдинОтступ - 86высотаХедера
		if (topOfWindow > paddingTopSecBlock) {
			var animationHeight = 104 - (topOfWindow - definitionSecondBlockHeight);
			if (animationHeight < 0) {
				secBlckFixedPos += animationHeight * 3;
			}
		}
		$('.animate-advantages').css('top', secBlckFixedPos);
		//-------------------------- SECOND BLOCK END --------------------------//

		/////////////////////////// THIRD BLOCK START ///////////////////////////
		if (topOfWindow >= 200 && showAnimation3block) {
			$('.main-third-block').removeClass('off').addClass('on');
			showAnimation3block = false;
		} else if (topOfWindow > 100 && topOfWindow < 200 && !showAnimation3block) {
			$('.main-third-block').removeClass('on').addClass('off');
			showAnimation3block = true;
		} else if (topOfWindow > 0 && topOfWindow < 50) {
			$('.main-third-block').removeClass('on, off')
		}
		if (topOfWindow > 375 && topOfWindow < 1000) {
			var speed1 = (topOfWindow - 375) * .1;
			$('.menu-btn').css('top', definitionBtnBlockUp - speed1);

			var speedWog = (topOfWindow - 375) * .4;
			$('#third-wog').css('top', definitionWogBlockDown - speedWog);

			var speedHamburger = (topOfWindow - 375) * .7;
			$('#third-hamburger').css('top', definitionHambBlockDown - speedHamburger);

			var speedBottle = (topOfWindow - 375) * .3;
			$('#third-bottle').css('top', definitionBotlBlockDown - speedBottle);
		}
		//-------------------------- THIRD BLOCK END --------------------------//

		/////////////////////////// FOUTH BLOCK START ///////////////////////////
		if (showAnimation && $('.main-fourth-block .row').offset().top < topOfWindow + $('.main-fourth-block').height()) {
			//console.warn(showAnimation);
			showAnimation = false;
			$('.main-fourth-block').addClass('animate-block-4');
		} else if (!showAnimation && $('.main-fourth-block .row').offset().top > topOfWindow + $('.main-fourth-block').height()) {
			//console.error(showAnimation);
			showAnimation = true;
			$('.main-fourth-block').removeClass('animate-block-4');
		}
		//-------------------------- FOUTH BLOCK END --------------------------//

	});
});
